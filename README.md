### Hi there 👋 I am [xyz8848](http://xyz8848.com/)!

## Skills 🚀
### Programming language
[![java](https://img.shields.io/badge/-java-blue?style=for-the-badge&logo=OpenJDK&logoColor=white)](https://github.com/xyz8848)
[![html](https://img.shields.io/badge/-html-blue?style=for-the-badge&logoColor=white)](https://github.com/xyz8848)

### Tools
[![idea](https://img.shields.io/badge/-idea-black?style=for-the-badge&logo=intellij-idea&logoColor=white)](https://github.com/xyz8848)
[![maven](https://img.shields.io/badge/-maven-black?style=for-the-badge&logo=apache-maven&logoColor=white)](https://github.com/xyz8848)
[![gradle](https://img.shields.io/badge/-gradle-black?style=for-the-badge&logo=gradle&logoColor=white)](https://github.com/xyz8848)
[![git](https://img.shields.io/badge/-git-black?style=for-the-badge&logo=git&logoColor=white)](https://github.com/xyz8848)
[![github](https://img.shields.io/badge/github-black?style=for-the-badge&logo=github&logoColor=white)](https://github.com/xyz8848)
[![markdown](https://img.shields.io/badge/-markdown-black?style=for-the-badge&logo=markdown&logoColor=white)](https://github.com/xyz8848)
[![json](https://img.shields.io/badge/-json-black?style=for-the-badge&logo=json&logoColor=white)](https://github.com/xyz8848)

## Stats 📊
#### GitHub Stats
[![xyz8848's GitHub stats](https://github-readme-stats.vercel.app/api?username=xyz8848&show_icons=true)](https://github.com/xyz8848)

#### Top Langs
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=xyz8848)](https://github.com/xyz8848)
